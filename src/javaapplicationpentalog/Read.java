package javaapplicationpentalog;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.*;

public class Read {
    

    public List<String> readFromFile(String fileName) { 
  
    List<String> lines = Collections.emptyList(); 
    try
    { 
      lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8); 
    } 
  
    catch (IOException e) 
    { 
       System.out.println("Something went wrong.");
    } 
    return lines; 
  } 
}
    
    

