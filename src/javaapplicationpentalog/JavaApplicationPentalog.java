/**
 *
 * @author Caciur Adina
 */
package javaapplicationpentalog;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JavaApplicationPentalog {

    public static void main(String[] args) {
        
        Read reader = new Read();
        
        try {
            List <String> results = reader.readFromFile("in.txt");
            File file = new File ("out.txt");
            BufferedWriter bw = new BufferedWriter (new FileWriter (file));
            PrintWriter pw = new PrintWriter (bw);
            Iterator<String> itr = results.iterator();
            while (itr.hasNext()){
                pw.println(itr.next());
            }
            bw.close();
        }
        catch (IOException ex){
            Logger.getLogger(JavaApplicationPentalog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
    

